#!/bin/sh
#
# Runs `go build` with flags configured for binary distribution. All
# it does differently from `go build` is burn git commit and version
# information into the binaries, so that we can track down user
# issues.
#
# If you're packaging Tailscale for a distro, please consider using
# this script, or executing equivalent commands in your
# distro-specific build system.
set -eux

echo "Building for $TARGETPLATFORM" 

case "$TARGETPLATFORM" in
	"linux/arm/v5"*)
		export CGO_ENABLED=0 GOOS=linux GOARCH=arm GOARM=5
		;;
	"linux/arm/v6"*)
		export CGO_ENABLED=0 GOOS=linux GOARCH=arm GOARM=6
		;;
	"linux/arm/v7"*)
		export CGO_ENABLED=0 GOOS=linux GOARCH=arm GOARM=7
		;;
	"linux/arm64"*)
		export CGO_ENABLED=0 GOOS=linux GOARCH=arm64 GOARM=7
		;;
	"linux/386"*)
		export CGO_ENABLED=0 GOOS=linux GOARCH=386
		;;
	"linux/amd64"*)
		export CGO_ENABLED=0 GOOS=linux GOARCH=amd64
		;;
	"linux/mips"*)
		export CGO_ENABLED=0 GOOS=linux GOARCH=mips
		;;
	"linux/mipsle"*)
		export CGO_ENABLED=0 GOOS=linux GOARCH=mipsle
		;;
	"linux/mips64"*)
		export CGO_ENABLED=0 GOOS=linux GOARCH=mips64
		;;
	"linux/mips64le"*)
		export CGO_ENABLED=0 GOOS=linux GOARCH=mips64le
		;;
	"linux/riscv64"*)
		export CGO_ENABLED=0 GOOS=linux GOARCH=riscv64
		;;
	*)
		echo "Unknown machine type: $machine"
		exit 1
esac

go="go"
if [ -n "${TS_USE_TOOLCHAIN:-}" ]; then
	go="./tool/go"
fi

eval `GOOS=$($go env GOHOSTOS) GOARCH=$($go env GOHOSTARCH) $go run ./cmd/mkversion`

if [ "$1" = "shellvars" ]; then
	cat <<EOF
VERSION_MINOR="$VERSION_MINOR"
VERSION_SHORT="$VERSION_SHORT"
VERSION_LONG="$VERSION_LONG"
VERSION_GIT_HASH="$VERSION_GIT_HASH"
EOF
	exit 0
fi

tags=""
ldflags="-X tailscale.com/version.longStamp=${VERSION_LONG} -X tailscale.com/version.shortStamp=${VERSION_SHORT}"

# build_dist.sh arguments must precede go build arguments.
while [ "$#" -gt 1 ]; do
	case "$1" in
	--extra-small)
		shift
		ldflags="$ldflags -w -s"
		tags="${tags:+$tags,}ts_omit_aws,ts_omit_bird,ts_omit_tap,ts_omit_kube"
		;;
	--box)
		shift
		tags="${tags:+$tags,}ts_include_cli"
		;;
	*)
		break
		;;
	esac
done

exec go build ${tags:+-tags=$tags} -ldflags "$ldflags" "$@"
